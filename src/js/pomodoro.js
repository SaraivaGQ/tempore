import PomodoroChart from './Components/PomodoroChart.js';

window.addEventListener('load', onLoad);

function onLoad() {
  const pomodoro = new PomodoroChart()
  const chart = document.querySelector("pomodoro-chart");
  const timer =  document.getElementById("pomodoro-time");
  const switchButton = document.getElementById("switchButton");
  const switchIco = document.getElementById("switch-ico");
  const switchWord = document.getElementById("switch-word");

  pomodoro.setElement(chart);
  pomodoro.setTimer(timer)
  pomodoro.render();

  switchButton.addEventListener("click", (event) => {
    event.preventDefault()
    if (pomodoro.isRunning()) {
      pomodoro.stop();
      switchWord.innerHTML = "Iniciar"
      switchIco.classList.add("fa-play")
      switchIco.classList.remove("fa-pause")
    }
    else {
      pomodoro.start();
      switchWord.innerHTML = "Parar"
      switchIco.classList.remove("fa-play")
      switchIco.classList.add("fa-pause")
    }
  })

}
