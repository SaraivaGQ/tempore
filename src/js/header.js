
export default class Header {
    isOpen = false;
    headerBars = document.getElementById('header-bars');
    headerNav = document.getElementById('header-nav');
    headerBarsIcon = document.getElementById('header-bars-icon');
    headerFilter = document.getElementById('header-filter');
    mainContainer = document.querySelector('.main-container');

    switch() {
        this.isOpen = !this.isOpen;

        if (this.isOpen) {
            this.headerBars.classList.add('open');
            this.headerNav.classList.add('open');
            this.headerFilter.classList.add('open');
            
            this.headerFilter.classList.remove('closed');
            this.headerBars.classList.remove('closed');
            this.headerNav.classList.remove('closed');
            this.headerBarsIcon.classList.remove('fa-bars');
            this.headerBarsIcon.classList.add('fa-times');
            console.log(document.querySelector('.main-container'))
            this.mainContainer.style.overflowY = 'hidden';
            this.mainContainer.style.maxHeight = "calc(100vh - 60px)";
            this.mainContainer.scrollTo(0);
        }
        else {
            this.headerFilter.classList.add('closed');
            this.headerBars.classList.add('closed');
            this.headerNav.classList.add('closed');
            
            this.headerFilter.classList.remove('open');
            this.headerBars.classList.remove('open');
            this.headerNav.classList.remove('open');
            this.headerBarsIcon.classList.add('fa-bars');
            this.headerBarsIcon.classList.remove('fa-times');
            this.mainContainer.style = null
        }
    }


}

