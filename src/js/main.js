import Header from './header.js'
import HeaderComponent from './Components/HeaderComponent.js';

loadHeaderComponent();
loadHeaderEvents();


function loadHeaderEvents() {
    var header = new Header();
    header.headerBars.addEventListener('click', () => {
        header.switch()
    })
    header.headerFilter.addEventListener('click', () => {
        header.switch();
    })
}

function loadHeaderComponent() {
    const HeaderObj = new HeaderComponent();
    HeaderObj.render();
}




