var modals = [...document.querySelectorAll('.tw-modal')];
var filters = [...document.querySelectorAll('.tw-modal-filter')];
var closers = [...document.querySelectorAll('.tw-close-modal')];

function CloseModal(element) {
    if([...element.classList].indexOf('close') !== -1) {
        element.classList.remove('close');
        element.classList.add('open');
    }
    else {
        element.classList.add('close');
        element.classList.remove('open');
    }
}

if (modals.length > 0) {
    filters.map((item, index) => {
        if (!modals[index]) return false;
        item.addEventListener('click', () => {
            CloseModal(modals[index])
        })
    })

    closers.map((item, index) => {
        if (!modals[index]) return false;
        item.addEventListener('click', () => {
            CloseModal(modals[index])
        })
    })
}


