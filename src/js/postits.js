const addBtn = document.getElementById('add-btn');
const mural = document.querySelector('mural');



function onPostItChange(id) {
  var postIts = localStorage.getItem('POST-ITS');
  if (postIts) {
    postIts = JSON.parse(postIts);
  }
  else {
    postIts = [];
  }

  var targetIndex = 0;
  postIts.map((p, i) => {
    if (p.id == id) {
      targetIndex = i;
    }
  })

  var value = document.getElementById('pt-content-'+id).innerHTML;
  var priority = document.getElementById('pt-select-'+id).value;

  postIts[targetIndex].value = value;
  postIts[targetIndex].priority = priority;
  
  localStorage.setItem('POST-ITS', JSON.stringify(postIts));
}

function onPostItPriorityChange(id) {
  var postIts = localStorage.getItem('POST-ITS');
  if (postIts) {
    postIts = JSON.parse(postIts);
  }
  else {
    postIts = [];
  }

  var targetIndex = 0;
  postIts.map((p, i) => {
    if (p.id == id) {
      targetIndex = i;
    }
  })

  var priority = document.getElementById('pt-select-'+id).value;

  postIts[targetIndex].priority = priority;
  
  localStorage.setItem('POST-ITS', JSON.stringify(postIts));

  render();
}

function deletePostIt(id) {
  var postIts = localStorage.getItem('POST-ITS');
  if (postIts) {
    postIts = JSON.parse(postIts);
  }
  else {
    postIts = [];
  }

  var targetIndex = 0;
  postIts.map((p, i) => {
    if (p.id == id) {
      targetIndex = i;
    }
  })
  postIts.splice(targetIndex, 1);
  
  localStorage.setItem('POST-ITS', JSON.stringify(postIts));
  render();
}


function addPI() {
  var postIts = localStorage.getItem('POST-ITS');
  if (postIts) {
    postIts = JSON.parse(postIts);
  }
  else {
    postIts = [];
  }

  postIts.push({
    value: "",
    id: postIts.length,
    priority: 'med'
  })
  localStorage.setItem('POST-ITS', JSON.stringify(postIts));
  render();
}

function render() {
  mural.innerHTML = "";
  var items = localStorage.getItem('POST-ITS');
  if (items) {
    items = JSON.parse(items);
  }
  else {
    items = [];
  }
  items.forEach(item => {
    var postIt = document.createElement('post-it');
    postIt.classList.add(item.priority || 'med')
    var header = document.createElement('post-it-header');
    var select = document.createElement('select');

    var option1 = document.createElement('option');
    option1.innerHTML = "Alta Prioridade";
    option1.value = 'high';
    select.add(option1);

    var option2 = document.createElement('option');
    option2.innerHTML = "Média Prioridade";
    option2.value = 'med';
    select.add(option2);

    var option3 = document.createElement('option');
    option3.innerHTML = "Baixa Prioridade";
    option3.value = 'low';
    select.add(option3);


    select.value = item.priority || 'med';
    select.onchange = () => onPostItPriorityChange(item.id);
    select.id = "pt-select-"+item.id;

    var deleteBtn = document.createElement('pt-delete');
    deleteBtn.innerHTML = "X";
    deleteBtn.onclick = () => deletePostIt(item.id);

    header.appendChild(select)
    header.appendChild(deleteBtn);
    postIt.appendChild(header);


    var content = document.createElement('post-it-content');
    content.contentEditable = true;
    content.onblur = () => onPostItChange(item.id);
    content.innerHTML = item.value;
    content.id = "pt-content-"+item.id;

    postIt.appendChild(content)
    mural.appendChild(postIt);
  })
  
}

addBtn.addEventListener('click', addPI);
console.log(addBtn)
render();