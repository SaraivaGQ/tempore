export default class PomodoroChart {
  
  secconds = 0;
  barWidth = 0;
  maxSecconds = 8700;
  element = null;
  barContainer = null;
  bar = null;
  whiteBar = null;
  interval = null;
  timer = null;
  running = false;

  convertToTimeString(secconds) {
    var date = new Date(null);
    date.setSeconds(parseInt(secconds));
    var result = date.toISOString().substr(11, 8);
    return result;
  }

  setSecconds(secconds) {
    this.secconds = secconds;
  }


  render() {
    this.renderBar()
    this.renderLines()
    this.renderClocks()
  }

  setElement(element) {
    this.element = element
  }
  setTimer(element) {
    this.timer = element
  }

  isRunning() {
    return this.running
  }

  start() {
    this.running = true;
    this.interval = setInterval(() => {
      this.secconds += 1

      this.barWidth = ((this.secconds / this.maxSecconds) % 1) * 100
      this.updateBar()
    }, 1000)
  }
  
  stop() {
    window.clearInterval(this.interval)
    this.interval = null;
    this.running = false;
  }

  updateBar() {
    this.timer.innerHTML = this.convertToTimeString(this.secconds)
    this.bar.style.width = this.barWidth.toFixed(2) + "%";
    this.whiteBar.style.width = (100 - this.barWidth).toFixed(2) + "%"
  }

  renderBar() {
    this.barContainer = document.createElement("pomodoro-bar-container")
    this.bar = document.createElement("pomodoro-bar")
    this.whiteBar = document.createElement("pomodoro-white-bar")
    this.barContainer.appendChild(this.bar)
    this.barContainer.appendChild(this.whiteBar)

    this.element.appendChild(this.barContainer)
  }

  renderLines() {
    const linesContainer = document.createElement("pomodoro-lines");
    for (let index = 0; index < 3; index++) {
      var line = document.createElement("pomodoro-line-item")
      var bar = document.createElement("pomodoro-blue-bar")
      line.appendChild(bar);
      linesContainer.appendChild(line)
    }
    var fLineCont = document.createElement("pomodoro-finish-line")
    var fLine = document.createElement("pomodoro-finish-line-item")
    var bar = document.createElement("pomodoro-blue-bar")
    fLine.appendChild(bar)
    fLineCont.appendChild(fLine)

    this.element.appendChild(linesContainer)
    this.element.appendChild(fLineCont)
  }

  renderClocks() {
    const container = document.createElement("pomodoro-lines");
    container.style.zIndex = 10
    for (let index = 0; index < 3; index++) {
      var clock = document.createElement("i")
      clock.classList.add("far")
      clock.classList.add("fa-clock")
      var line = document.createElement("pomodoro-line-item");
      var clockTime = document.createElement("clock-time");
      clockTime.innerHTML = "5min";
      line.appendChild(clock);
      line.appendChild(clockTime);

      container.appendChild(line)
    }

    var fLineCont = document.createElement("pomodoro-finish-line")
    var fLine = document.createElement("pomodoro-finish-line-item")
    var clock = document.createElement("i")
    
    var clockTime = document.createElement("clock-time");
    clockTime.innerHTML = "30min";

    clock.classList.add("far")
    clock.classList.add("fa-clock")
    fLine.appendChild(clock)
    fLine.appendChild(clockTime);
    fLineCont.appendChild(fLine)
    fLineCont.style.zIndex = 10

    this.element.appendChild(container)
    this.element.appendChild(fLineCont)
  }
}