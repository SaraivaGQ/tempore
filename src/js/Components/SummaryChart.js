

export default class SummaryChart {
    data = {};
    
    pageId = Object.keys(this.data).indexOf(this.getTodaysDayFormat())
    dataPage = this.getTodaysDayFormat()
    targetParent = null;
    leftTargetParent = null;
    rightTargetParent = null;
    bottomTargetParent = null;
    element;
    colors = [
        "#0090ff",
        "#189f65",
        "#ff0077",
        "#0f7",
        "#ff6000",
        "#b400ff"
    ];
    hourList = ["0:00", "2:00", "4:00", "6:00", "8:00", "10:00", "12:00", "14:00", "16:00", "18:00", "20:00", "22:00"];
    

    constructor() {
        if (!this.data[this.getTodaysDayFormat()]) {
            this.data[this.getTodaysDayFormat()] = {}
            this.pageId = Object.keys(this.data).indexOf(this.getTodaysDayFormat())
        }
        document.getElementById('summary-chart-date-label').innerHTML = this.dataPage
    }


    getTodaysDayFormat(d = null) {
        var date = d || new Date();
        return `${date.getDate() < 10 ? "0" : ""}${date.getDate()}/${date.getMonth()+1 < 10 ? "0" : ""}${date.getMonth() + 1}/${date.getFullYear()}`;
    }

    // Setters
    setParent(element) {
        this.targetParent = element;
        this.leftTargetParent = document.createElement('summary-chart-left');
        this.rightTargetParent = document.createElement('summary-chart-right');
        this.bottomTargetParent = document.createElement('summary-chart-bottom');
        this.targetParent.appendChild(this.leftTargetParent);
        this.targetParent.appendChild(this.rightTargetParent);
        this.targetParent.appendChild(this.bottomTargetParent);
    }
    setData(data) {
        this.data = data;
    }



    render() {
        console.log(this.dataPage)
        window.addEventListener('resize', () => {
            this.resizeChart();
        });
        this.draw();
        this.resizeChart();
    }

    resizeChart() {
        this.targetParent.innerHTML = null;
        this.setParent(this.targetParent);
        this.draw();
    }

    draw() {
        this.element = document.createElement('summarychart-component');
        this.leftTargetParent.appendChild(this.element)

        this.drawLines();
        this.drawBars();
        this.drawNumbers();
        this.drawLabels();
    }

    drawLines() {
        var element = document.createElement("chart-lines");
        for (let i = 0; i < 13; i++) {
            var item = document.createElement("chart-line-item");
            element.appendChild(item);
        }
        return this.element.appendChild(element)
    }

    drawBars() {
        var colorCounter = 0;
        Object.keys(this.data[this.dataPage]).map((key, index) => {
            this.data[this.dataPage][key].map(item => {
                var element = document.createElement('chart-bar');
                element.style.top = `${index * (this.element.offsetHeight / Object.keys(this.data[this.dataPage]).length) + 10}px`;
                element.style.left = `${this.element.offsetWidth / 24 * item.from}px`;
                element.style.width = `${this.element.offsetWidth / 24 * (item.to - item.from)}px`;
                element.style.height = `${15}px`;
                element.style.backgroundColor = this.colors[colorCounter];
                element.setAttribute("data-description", key)
                this.element.appendChild(element)
            })
            if (colorCounter == this.colors.length - 1) {
                colorCounter = 0;
            }
            else {
                colorCounter++;
            }
        })
    }


    drawNumbers() {
        this.hourList.map((hour, index) => {
            var element = document.createElement('chart-hour');
            element.innerHTML = hour;
            element.style.left = `${this.leftTargetParent.offsetWidth / this.hourList.length * index}px`;
            this.bottomTargetParent.appendChild(element)
        })
    }

    drawLabels() {
        var colorCounter = 0;
        Object.keys(this.data[this.dataPage]).map(key => {
            var element = document.createElement('chart-label');
            var ball = document.createElement('label-color');
            var text = document.createElement('label-text');
            ball.style.backgroundColor = this.colors[colorCounter];
            text.innerHTML = key;
            element.appendChild(ball);
            element.appendChild(text)

            this.rightTargetParent.appendChild(element)

            if (colorCounter == this.colors.length - 1) {
                colorCounter = 0;
            }
            else {
                colorCounter++;
            }
        })
    }


    nextPage() {
        if (Object.keys(this.data)[this.pageId + 1]) {
            this.pageId += 1;
            this.dataPage =  Object.keys(this.data)[this.pageId];
            this.resizeChart();
            document.getElementById('summary-chart-date-label').innerHTML = this.dataPage
        }
        else {
            return false;
        }
    }
    prevPage() {
        if (Object.keys(this.data)[this.pageId - 1]) {
            this.pageId -= 1;
            this.dataPage =  Object.keys(this.data)[this.pageId];
            this.resizeChart();
            document.getElementById('summary-chart-date-label').innerHTML = this.dataPage
        }
        else {
            return false;
        }
    }
}