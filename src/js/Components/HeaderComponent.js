
export default class Header {

  headerElement = null;

  routes = [
    {name: 'Home', to: 'home.html'},
    {name: 'Pomodoro', to: 'pomodoro.html'},
    {name: 'Tarefas', to: 'tarefas.html'},
    {name: 'Timer', to: 'timer.html'},
    {name: 'Post-Its', to: 'post-its.html'},
  ]

  homeRoute = this.routes[0];

  logo = './src/images/logo.png'


  render() {
    this.headerElement = document.querySelector('header');
    this.headerElement.innerHTML = "";

    // Bars
    this.loadBars();

    // Logo
    this.loadLogo();

    // Filter
    this.loadFilter();

    // Nav
    this.loadNav();
  }

  loadBars() {
    let barsDiv = document.createElement('div');
    barsDiv.id = 'header-bars';
    let bars = document.createElement('i');
    bars.classList.add('fas');
    bars.classList.add('fa-bars');
    bars.id = 'header-bars-icon';
    barsDiv.appendChild(bars);
    this.headerElement.appendChild(barsDiv);
  }

  loadLogo() {
    let a = document.createElement('a');
    a.href = this.homeRoute.to;
    let logo = document.createElement('img');
    logo.id = 'header-logo';
    logo.src = this.logo;
    a.appendChild(logo);
    this.headerElement.appendChild(a);
  }
  
  loadFilter() {
    let filter = document.createElement('div');
    filter.id = 'header-filter';
    this.headerElement.appendChild(filter);
  }

  loadNav() {
    var nav = document.createElement('nav');
    nav.id = 'header-nav';

    this.routes.forEach(link => {
      var a = document.createElement('a');
      a.href = link.to;
      a.innerHTML = link.name;
      nav.appendChild(a);
    })

    this.headerElement.appendChild(nav);
  }
}