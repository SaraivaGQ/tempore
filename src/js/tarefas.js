 
var btnAddTimer = document.getElementById('add-timer-btn');
var modalContainer = document.querySelector(".tw-modal");
var modalNome = document.getElementById('modal-nome');
var modalMin = document.getElementById('modal-min');
var modalDesc = document.getElementById('modal-desc');
var form = document.getElementById('form-add');
var cancel = document.getElementById('cancel-add');
var container = document.querySelector('tarefas');
var headerFilter = document.getElementById('header-filter-input');



btnAddTimer.addEventListener('click', (event) => {
  event.preventDefault();  
  switchModal();
});

function switchModal() {
  if([...modalContainer.classList].indexOf('close') !== -1) {
      modalContainer.classList.remove('close');
      modalContainer.classList.add('open');
  }
  else {
      modalContainer.classList.add('close');
      modalContainer.classList.remove('open');
  }
}



function addTarefa(event) {
  event.preventDefault();
  var values = {
    nome: modalNome.value,
    segundos: modalMin.value * 60,
    description: modalDesc.value,
    feito: 0
  }
  var oldT = localStorage.getItem('TAREFAS');
  if (oldT) {
    var t = JSON.parse(oldT);
    t.push(values);
    localStorage.setItem('TAREFAS', JSON.stringify(t));
  }
  else {
    localStorage.setItem('TAREFAS', JSON.stringify([values]));
  }
  modalNome.value = "";
  modalMin.value = "";
  modalDesc.value = "";
  switchModal();
  loadTarefas();
}


function loadTarefas(filter = null) {
  var tarefas = [];
  var tstring = localStorage.getItem('TAREFAS');
  if (tstring) {
    tarefas = JSON.parse(tstring);
  }
  console.log(filter)

  if (filter) {
    tarefas = tarefas.filter(t => t.nome.toUpperCase().indexOf(filter.toUpperCase()) != -1)
  }

  console.log(tarefas)
  container.innerHTML = ""
  tarefas.forEach((register, index) => {
    var item = document.createElement('tarefa-item');
    item.id = "item-"+index;

    // Top
    var title = document.createElement('item-title');
    title.innerHTML = register.nome;
    title.title = register.nome;
    var a = document.createElement('item-delete');
    a.innerHTML = "X";
    a.onclick = () => {deleteTarefa(index)}
    title.appendChild(a)
    item.appendChild(title);


    // Center
    var description = document.createElement('item-description');
    description.innerHTML = register.description || "";
    description.title = register.description || "";
    item.appendChild(description);

    // Bottom
    var objectives = document.createElement('item-objectives');
    var span1 = document.createElement('span');
    span1.innerHTML = `Objetivo: ${convertToTimeString(register.segundos)}`
    objectives.appendChild(span1);
    item.appendChild(objectives);


    container.appendChild(item);
  })
}

function deleteTarefa(id) {
  var tarefas = [];
  var tstring = localStorage.getItem('TAREFAS');
  if (tstring) {
    tarefas = JSON.parse(tstring);
  }
  tarefas.splice(id, 1);

  localStorage.setItem('TAREFAS', JSON.stringify(tarefas));
  loadTarefas();
}

function convertToTimeString(secconds) {
  var date = new Date(null);
  date.setSeconds(parseInt(secconds));
  var result = date.toISOString().substr(11, 8);
  return result;
}

cancel.addEventListener('click', switchModal);
form.addEventListener('submit', addTarefa);
headerFilter.addEventListener('input', () => {
  loadTarefas(headerFilter.value);
})

loadTarefas();