import SummaryChart from './Components/SummaryChart.js'

var modalContainer = document.querySelector(".tw-modal");
var timerContainer = document.getElementById('timers');
var btnAddTimer = document.getElementById('add-timer-btn');
var btnSubmitAddTimer = document.getElementById('submit-add-timer');
var btnCancelAddTimer = document.getElementById('cancel-add-timer');
var chartBtnLeft = document.getElementById("summary-chart-btn-left");
var chartBtnRight = document.getElementById("summary-chart-btn-right");
var modalImpDe = document.getElementById('modal-imp-de');
var modalImpAte = document.getElementById('modal-imp-ate');
var modalLblAte = document.getElementById('modal-lbl-ate')
var modalImpAcabou = document.getElementById('modal-imp-acabou');
var modalImpTipo = document.getElementById('modal-imp-tipo');

var CHART = null;

var interval = null;
var timers = [
];


function convertToTimeString(secconds) {
    var date = new Date(null);
    date.setSeconds(parseInt(secconds));
    var result = date.toISOString().substr(11, 8);
    return result;
}


function renderTimes() {
    var html = '';

    timers.map((time, index) => {
        var secs = 0;
        if (time.time) {
            secs = time.time;
        }
        else {
            secs = (new Date().getTime() - time.start.getTime()) / 1000;
            timers[index].time = secs;
        }
        
        html += `
                <div class="timer-item" data-chart-data='${JSON.stringify(time.chartData)}'>
                    <span class="timer-title">${time.name}</span>
                    <span class="timer-time" data-value="${secs}">${convertToTimeString(secs)}</span>
                    <button class="timer-button"><i class="fas fa-square-full"></i></button>
                </div>
        `
    });
    if (timers.length === 0) {
        html += "<span class='info-span'>Você não tem nenhum timer ativo</span>"
    }
    
    timerContainer.innerHTML = html;
    var buttons = [...document.querySelectorAll('.timer-button')];
    var times = [...document.querySelectorAll('.timer-item')];
    buttons.map((item, index) => {
        item.addEventListener('click', event => {
            var json = JSON.parse(times[index].getAttribute('data-chart-data'));
            finishTimer(index, json)
        })
    })
}

function updateTimes() {
    var times = [...document.querySelectorAll('.timer-time')];
    times.map((item, index) => {
        var secs = parseFloat(item.getAttribute('data-value'));
        item.setAttribute('data-value', secs+1);
        item.innerHTML = convertToTimeString(secs+1)
    })
}

function finishTimer(index, json) {
    console.log(index)
    var t1 = new Date(json.from)
    var t1Mins = t1.getMinutes() / 60
    var t2 = new Date()
    var t2Mins = t2.getMinutes() / 60

    var data = {
        from: t1.getHours() + t1Mins,
        to: t2.getHours() + t2Mins
    }
    if (!CHART.data[CHART.getTodaysDayFormat(t1)]) {
        CHART.data[CHART.getTodaysDayFormat(t1)] = {}
    }
    if (!CHART.data[CHART.getTodaysDayFormat(t1)][json.type]) {
        CHART.data[CHART.getTodaysDayFormat(t1)][json.type] = []
    }
    CHART.data[CHART.getTodaysDayFormat(t1)][json.type].push(data)
    localStorage.setItem("CHART-DATA", JSON.stringify(CHART.data))
    CHART.resizeChart()
    timers.splice(index, 1)
    renderTimes()
}


function switchModal() {
    if([...modalContainer.classList].indexOf('close') !== -1) {
        modalContainer.classList.remove('close');
        modalContainer.classList.add('open');
    }
    else {
        modalContainer.classList.add('close');
        modalContainer.classList.remove('open');
    }
}

function loadCharts() {
    var summaryCharts = [...document.querySelectorAll('summary-chart')];
    summaryCharts.map(element => {
        console.log(element)
        CHART = new SummaryChart();
       
        CHART.setParent(element);
        
        CHART.render()
        var data = localStorage.getItem('CHART-DATA');
        if (data) {
            CHART.setData(JSON.parse(data))
            CHART.drawBars();
            CHART.drawLabels();
        }
    })
}

function loadHome() {
    loadCharts();
    renderTimes();
    
    interval = setInterval(() => {
        timers.map((item, index) => {
            timers[index].time = item.time + 1;
        });
        updateTimes();
    }, 1000);
    
    
    btnAddTimer.addEventListener('click', (event) => {
        event.preventDefault();
        
        switchModal();
    });

    btnSubmitAddTimer.addEventListener('click', event => {
        event.preventDefault();

        var values = getModalValues();
        if (values) {
            timers.push({start: new Date(values.de), name: values.tipo, time: null, chartData: {date: values.date, type: values.tipo, from: new Date(values.de), to: 0}})
            renderTimes();
            console.log(values)
        }
        
        switchModal();
    })

    btnCancelAddTimer.addEventListener('click', event => {
        event.preventDefault();
        switchModal();
    })

    chartBtnLeft.addEventListener('click', event => {
        CHART.prevPage();

    })
    chartBtnRight.addEventListener('click', event => {
        CHART.nextPage();
    })

    modalImpAcabou.addEventListener('change', event => {
        if (event.target.checked) {
            modalLblAte.style = "display: flex"
            btnSubmitAddTimer.innerHTML = "Cadastrar Tempo"
        }
        else {
            
            modalLblAte.style = "display: none"
            btnSubmitAddTimer.innerHTML = "Iniciar Contador"
        }
    })
    var dt = new Date();
    modalImpDe.value = `${dt.getFullYear()}-${dt.getMonth()+1 < 10 ? "0" : ""}${dt.getMonth()+1}-${dt.getDate() < 10 ? "0" : ""}${dt.getDate()}T${dt.getHours() < 10 ? "0" : ""}${dt.getHours()}:${dt.getMinutes() < 10 ? "0" : ""}${dt.getMinutes()}:${dt.getSeconds() < 10 ? "0" : ""}${dt.getSeconds()}`
    modalImpAte.value =  `${dt.getFullYear()}-${dt.getMonth()+1 < 10 ? "0" : ""}${dt.getMonth()+1}-${dt.getDate() < 10 ? "0" : ""}${dt.getDate()}T${dt.getHours() < 10 ? "0" : ""}${dt.getHours()}:${dt.getMinutes() < 10 ? "0" : ""}${dt.getMinutes()}:${dt.getSeconds() < 10 ? "0" : ""}${dt.getSeconds()}`
}

function getModalValues() {
    var values = {}
    var labels = [...document.querySelector('.tw-modal-form-inputs').children];
    labels.map(label => {
        var input = label.children[0];
        values[input.name] = input.type == "checkbox" ? input.checked : input.value;
    })

    if (values.acabou) {
        var firstDate = new Date(values.de)
        var seccondDate = new Date(values.ate)

        var f1Mins = firstDate.getMinutes() / 60
        var f2Mins = seccondDate.getMinutes() / 60

        var data = {
            from: firstDate.getHours() + f1Mins,
            to: seccondDate.getHours() + f2Mins
        }
        if (!CHART.data[CHART.getTodaysDayFormat(firstDate)]) {
            CHART.data[CHART.getTodaysDayFormat(firstDate)] = {}
        }
        if (!CHART.data[CHART.getTodaysDayFormat(firstDate)][values.tipo]) {
            CHART.data[CHART.getTodaysDayFormat(firstDate)][values.tipo] = []
        }
        CHART.data[CHART.getTodaysDayFormat(firstDate)][values.tipo].push(data)
        
        localStorage.setItem("CHART-DATA", JSON.stringify(CHART.data))
        CHART.resizeChart()
        return false;
    }
    else {

        var firstDate = new Date(values.de)
        values.date = CHART.getTodaysDayFormat(firstDate);
        return values;

    }
}

function loadTypes() {
  var tarefas = [];
  var tstring = localStorage.getItem('TAREFAS');
  if (tstring) {
    tarefas = JSON.parse(tstring);
  }
  tarefas.forEach(item => {
      var option = document.createElement('option');
      option.value = item.nome;
      option.innerHTML = item.nome;
      modalImpTipo.appendChild(option)

  })
}



window.addEventListener('load', onLoad);


function onLoad() {
    loadHome();
    loadTypes();
}
